Installation:

PBRTInterface.py should be placed in one of the Maya plug-in directories. The location of these directories can be found in the Window -> Settings/Preferences -> Plug-in Manager screen, on my computer this is /Users/Shared/Autodesk/maya/plug-ins but this will vary.
Once the plugin is placed in the correct directory it can be loaded using the same screen mentioned above, nearly tick the loaded box.

Exporter usage:

Rendering and exporting the scene can now easily be performed using the pbrtexport and pbrtexportandrender MEL commands, the latter requires that the render window be already open.

To change the location the exporter writes to the PBRTInterface.py file must be modified, a nicer way to specify this will be implemented at a later date.

Change the lines that look like the following:

# modify the following line to change the output file path and image format/size
kDefaultExportSpec = PBRTXOutputSpec('/Users/melby/Desktop/out.txt', "file.tga", 640, 480)

Material Usage:
At this point if you select a mesh in the scene and type in one of the following MEL commands:pbrtapplymattepbrtapplyglasspbrtapplyplanetpbrtapplyuvA new surface shader will be added to the specified mesh, from here if you navigate to the plug connected to "Out Color" the settings for this material will be visible and modifiable.As the name of the aforementioned commands might suggest, these apply matte, glass, planet and uv texture/materials respectively.