import maya.cmds as cmds
import maya.OpenMaya as OM
import maya.OpenMayaUI as OMUI
import maya.OpenMayaRender as OMR
import sys
import math
from time import time
import os
from subprocess import Popen, PIPE
import errno
from threading import Thread
import maya.mel
import maya.OpenMayaMPx as OMPx
import maya.OpenMayaFX as OMFx
import maya.OpenMayaAnim as OMA

################################################
# global variables
################################################

PBRTXTextureCount = 0
PBRTXIndent = 0

################################################
# helper functions
################################################

def cIndent():
	global PBRTXIndent
	PBRTXIndent = PBRTXIndent + 1

def cUnindent():
	global PBRTXIndent
	PBRTXIndent = PBRTXIndent - 1

def cWrite(file, string):
	global PBRTXIndent
	file.write('%s%s' % ('\t' * PBRTXIndent, string))

def cWritePartial(file, string):
	file.write(string)

def rad2deg(radians):
	degrees = 180 * radians / math.pi
	return degrees
def deg2rad(degrees):
	radians = math.pi * degrees / 180
	return radians
	
def isVisible(fnDag):
	"""
	Summary:	determines if the given DAG node is currently visible
	Args	:	fnDag - the DAG node to check
	Returns:	true if the node is visible;		
				false otherwise
	"""
	if fnDag.isIntermediateObject():
		return False

	try:
		visPlug = fnDag.findPlug("visibility")
		return visPlug.asBool()
	except:
		OM.MGlobal.displayError("MPlug::findPlug")
		return False
		
def matrixToArray(matrix):
	A = [[],[],[],[]]
			
	for i in xrange(4):
		for j in xrange(4):
			A[i].append(matrix(i, j))
	
	return A

def intPtr():
	util = OM.MScriptUtil()
	return (util.asIntPtr(), util)

def intFromPtr(ptr):
	return OM.MScriptUtil.getIntArrayItem(ptr[0], 0)
	
def uintPtr():
	util = OM.MScriptUtil()
	return (util.asUintPtr(), util)
	
def uintFromPtr(ptr):
	return OM.MScriptUtil.getUintArrayItem(ptr[0], 0)
	
def float2Ptr():
	util = OM.MScriptUtil()
	return (util.asFloat2Ptr(), util)

class PBRTXFloat2:
	def __init__(self, x, y):
		self.x = x
		self.y = y

def float2FromPtr(ptr):
	return PBRTXFloat2(OM.MScriptUtil.getFloat2ArrayItem(ptr[0], 0, 0), OM.MScriptUtil.getFloat2ArrayItem(ptr[0], 0, 1))

def float3Ptr():
	util = OM.MScriptUtil()
	return (util.asFloat3Ptr(), util)

class PBRTXFloat3:
	def __init__(self, x, y, z):
		self.x = x
		self.y = y
		self.z = z

def float3FromPtr(ptr):
	return PBRTXFloat3(OM.MScriptUtil.getFloat3ArrayItem(ptr[0], 0, 0), OM.MScriptUtil.getFloat3ArrayItem(ptr[0], 0, 1), OM.MScriptUtil.getFloat3ArrayItem(ptr[0], 0, 2))

def floatPtr():
	util = OM.MScriptUtil()
	return (util.asFloatPtr(), util)

def floatFromPtr(ptr):
	return OM.MScriptUtil.getFloatArrayItem(ptr[0], 0)

def doublePtr():
	util = OM.MScriptUtil()
	return (util.asDoublePtr(), util)

def doubleFromPtr(ptr):
	return OM.MScriptUtil.getDoubleArrayItem(ptr[0], 0)

def writeTransformationMatrix(file, dagPath):
	try:
		matrix = matrixToArray(dagPath.inclusiveMatrix())
					
		cWrite(file, 'ConcatTransform [%+f %+f %+f %+f %+f %+f %+f %+f %+f %+f %+f %+f %+f %+f %+f %+f]\n' % (matrix[0][0], matrix[0][1], matrix[0][2], matrix[0][3],
																							matrix[1][0], matrix[1][1], matrix[1][2], matrix[1][3],
																							matrix[2][0], matrix[2][1], matrix[2][2], matrix[2][3],
																							matrix[3][0], matrix[3][1], matrix[3][2], matrix[3][3]))
	
	
	except:
		OM.MGlobal.displayError("Error in outputting transformattion")
		raise

################################################
# export helper classes
################################################

class ExportError:
	def __init__(self, reason):
		self.reason = reason

class PBRTXOutputSpec:
	def __init__(self, path, imageName, width, height):
		self.path = path
		self.imageName = imageName
		self.width = width
		self.height = height

################################################
# constants
################################################

kMattePBRTShader = "PBRTMatteShader"
kMattePBRTShaderNodeId = OM.MTypeId(0x87100)
kGlassPBRTShader = "PBRTGlassShader"
kGlassPBRTShaderNodeId = OM.MTypeId(0x87101)
kPlanetPBRTShader = "PBRTPlanetShader"
kPlanetPBRTShaderNodeId = OM.MTypeId(0x87102)
kUVPBRTShader = "PBRTUVShader"
kUVPBRTShaderNodeId = OM.MTypeId(0x87103)
kDifferentialMattePBRTShader = "PBRTDifferentialMatteShader"
kDifferentialMattePBRTShaderNodeId = OM.MTypeId(0x87200)

kPluginNodeClassify = 'utility/general'

# modify the following line to change the output file path and image format/size

#outputFileDirectory = 'C:/Users/Alice/Desktop/outputs/'
outputFileDirectory = '/Users/melby/Desktop/export/'

kDefaultExportSpec = PBRTXOutputSpec(outputFileDirectory+"out.txt", "file.tga", 640, 480)

def kAnimatedExportSpec(frame):
	frameFileName = outputFileDirectory + 'out' + str(frame+1) + '.txt'
	return PBRTXOutputSpec(frameFileName, "file.tga", 640, 480)

# material parameters
kDefaultKd = (0.5, 0.5, 0.5)
kDefaultSigma = 0
kMinSigma = 0
kMaxSigma = 360
kDefaultKr = (1.0, 1.0, 1.0)
kDefaultKt = (1.0, 1.0, 1.0)
kDefaultIOR = 1.5
kMinIOR = 0.0
kMaxIOR = 10.0

kPlanetPBRTShaderFloatKeys = 		['heightscale',	'snowline',	'iceextent', 	'oceandepth',	'forestcover',	'mountainheight',	'terrainheight',	'hillheight',	'seed']
kPlanetPBRTShaderFloatDefaults =	[0.2,			0.25,		0.8,			0.5,			0.5,			0.6,				0.2,				0.2,			0.0]
kPlanetPBRTShaderFloatMins = 		[0.0,			0.0,		0.0,			0.0,			0.0,			0.0,				0.0,				0.0,			0.0]
kPlanetPBRTShaderFloatMaxes = 		[1.0,			1.0,		1.0,			1.0,			1.0,			1.0,				1.0,				1.0,			10000.0]

kUVPBRTShaderFloatKeys =		['uscale',	'vscale']
kUVPBRTShaderFloatDefaults =	[1.0,		1.0]
kUVPBRTShaderFloatMins =		[0.0,		0.0]
kUVPBRTShaderFloatMaxes =		[100.0,		100.0]

################################################
# camera
################################################

class PBRTXCamera:
	def __init__(self, spec):
		self.actualRatio = (spec.width/float(spec.height))
		self.camera = OM.MFnCamera()
		self.getActiveCamera()
	
	def getActiveCamera(self):
		"""
		Query's maya's windowing system for the current camera being selected
		and used by the user
		"""
		# Get the current camera
		curView = OMUI.M3dView.active3dView()
		self.camDagPath = OM.MDagPath()
		curView.getCamera(self.camDagPath)
				
		# construct the camera data for pbrt
		self.camera.setObject(self.camDagPath)
	
	
	def writeLookAt(self, file):
		# utilizes the lookat points so we don't have to worry about handedness (too much)
		try:
			eye = self.camera.eyePoint(OM.MSpace.kWorld)
		except:
			raise ExportError("Could not get eye point")
			
		try:
			up = self.camera.upDirection(OM.MSpace.kWorld)
		except:
			raise ExportError("Could not get up vector")
		
		try:
			at = self.camera.centerOfInterestPoint(OM.MSpace.kWorld)
		except:
			raise ExportError("Could not get at point")
		
		cWrite(file, 'LookAt %+f %+f %+f %+f %+f %+f %+f %+f %+f \n' % (eye.x, eye.y, eye.z,
																		at.x, at.y, at.z,
																		up.x, up.y, up.z))	

	def writeCommon(self, file):
		if self.camera.isDepthOfField():
			cWritePartial(file, '"float focaldistance" [%+f] ' % (self.camera.focusDistance()))
			# there doesn't appear to be a way to get this attribute from python - probably named something different in the api docs
			focusRegionScale = maya.mel.eval('getAttr '+self.camDagPath.fullPathName()+'.focusRegionScale;')
			cWritePartial(file, '"float lensradius" [%+f] ' % (self.camera.fStop()/focusRegionScale))
		cWritePartial(file, '"float frameaspectratio" [%+f] ' % self.actualRatio)

	def writeOrtho(self, file):
		"""
		creates pbrt commands for inputting orthographic camera
		parts based off http://src.luxrender.net/luxblend/rev/f5a89126ab9b
		"""
		self.writeLookAt(file)
		cWrite(file, 'Camera "orthographic" ')
		self.writeCommon(file)

		screenWindow = None
		if self.camera.filmFit() == OM.MFnCamera.kHorizontalFilmFit:
			screenWindow = [(-self.camera.orthoWidth()/2),
						(self.camera.orthoWidth()/2),
						(-(self.camera.orthoWidth()/self.actualRatio)/2),
						((self.camera.orthoWidth()/self.actualRatio)/2)]
		elif self.camera.filmFit() == OM.MFnCamera.kVerticalFilmFit:
			screenWindow = [(-self.camera.orthoWidth()*self.actualRatio/2),
						(self.camera.orthoWidth()*self.actualRatio/2),
						(-(self.camera.orthoWidth())/2),
						((self.camera.orthoWidth())/2)]
		else:
			screenWindow = [(-self.camera.orthoWidth()*self.actualRatio/2),
						(self.camera.orthoWidth()*self.actualRatio/2),
						(-(self.camera.orthoWidth())/2),
						((self.camera.orthoWidth())/2)]
			OM.MGlobal.displayWarning("Unsupported camera fit resolution gate setting, using fit vertical")

		cWritePartial(file, '"float screenwindow" [%+f %+f %+f %+f] ' % (screenWindow[0], screenWindow[1], screenWindow[2], screenWindow[3]))
		cWritePartial(file, '\n')

	def writePerspective(self, file):
		"""
		creates pbrt commands for inputting perspective camera
		"""
		self.writeLookAt(file)
		cWrite(file, 'Camera "perspective" ')
		self.writeCommon(file)

		fitCorrection = 1
		if self.camera.filmFit() == OM.MFnCamera.kHorizontalFilmFit:
			fitCorrection = self.camera.aspectRatio()/self.actualRatio
		elif self.camera.filmFit() == OM.MFnCamera.kVerticalFilmFit:
			fitCorrection = 1
		else:
			fitCorrection = 1
			OM.MGlobal.displayWarning("Unsupported camera fit resolution gate setting, using fit vertical")

		cWritePartial(file, '"float fov" [%+f] ' % (rad2deg(self.camera.verticalFieldOfView())*fitCorrection))
		cWritePartial(file, '\n')
	
	def writeToFile(self, file):
		if self.camera.isOrtho():
			self.writeOrtho(file)		
		else:
			self.writePerspective(file)

	
################################################
# lights
################################################

class PBRTXLight:
	def __init__(self, dagPath):
		self.dagPath = OM.MDagPath(dagPath)
			
	def writeToFile(self, file):
		cWrite(file, 'TransformBegin\n')
		cIndent()
		writeTransformationMatrix(file, self.dagPath)
		self.internalWriteToFile(file)
		cUnindent()
		cWrite(file, 'TransformEnd\n')

class PBRTXLightSpotLight(PBRTXLight):
	def __init__(self, dagPath):
		PBRTXLight.__init__(self, dagPath)
		self.light = OM.MFnSpotLight(dagPath)
		
	def internalWriteToFile(self, file):
		# get the color data
		color = self.light.color()
		intensity = self.light.intensity()
		
		# output the light source
		cWrite(file, 'LightSource "spot" ')
		cWritePartial(file, '"color I" [%+f %+f %+f] ' % (color.r*intensity, color.g*intensity, color.b*intensity))
		cWritePartial(file, '"point from" [0 0 0] "point to" [0 0 -1] ')
		cWritePartial(file, '"float coneangle" [%+f] ' % (rad2deg(self.light.coneAngle()/2 + self.light.penumbraAngle()*2)))
		cWritePartial(file, '"float conedeltaangle" [%+f]\n' % rad2deg(self.light.penumbraAngle()*2))
		
class PBRTXLightPointLight(PBRTXLight):
	def __init__(self, dagPath):
		PBRTXLight.__init__(self, dagPath)
		self.light = OM.MFnPointLight(dagPath)

	def internalWriteToFile(self, file):
		# get the color data
		color = self.light.color()
		intensity = self.light.intensity()
		
		# output the light source
		cWrite(file, 'LightSource "point" ')
		cWritePartial(file, '"color I" [%+f %+f %+f]\n' % (color.r*intensity, color.g*intensity, color.b*intensity))

class PBRTXLightDirectionalLight(PBRTXLight):
	def __init__(self, dagPath):
		PBRTXLight.__init__(self, dagPath)
		self.light = OM.MFnDirectionalLight(dagPath)
		
	def internalWriteToFile(self, file):
		# get the color data
		color = self.light.color()
		intensity = self.light.intensity()
		
		# output the light source
		cWrite(file, 'LightSource "distant" ')
		cWritePartial(file, '"color L" [%+f %+f %+f] ' % (color.r*intensity, color.g*intensity, color.b*intensity))
		cWritePartial(file, '"point from" [0 0 0] "point to" [0 0 -1]\n')

def lightFactory(dagPath):
	"""
	factory which determines all available light types and will return the
	appropriate light to use in exporting
	"""
	light = OM.MFnLight(dagPath)
	
	try:
		return PBRTXLightSpotLight(dagPath)
	except:
		pass
	
	try:
		return PBRTXLightPointLight(dagPath)
	except:
		pass

	try:
		return PBRTXLightDirectionalLight(dagPath)
	except:
		pass
	
	# todo support other lights

	OM.MGlobal.displayWarning("Light type not supported")
		
	return None
	
################################################
# materials
################################################

class PBRTXMaterialStorage:
	"""
	Holds the information for a material.
	"""
	def __init__( self, color=[1.0, 1.0, 1.0], alpha=1.0, specular=[0.0, 0.0, 0.0], emissive=[0.0, 0.0, 0.0], diffuseCoefficient=1.0, roughness=0.0, texture='', reflection=[0.0, 0.0, 0.0], bumpmap=''):
		self.color = color
		self.alpha = alpha
		self.specular = specular
		self.emissive = emissive
		self.diffuseCoefficient = diffuseCoefficient
		self.roughness = roughness
		self.texture = texture
		self.reflection = reflection
		self.bumpmap = bumpmap

class PBRTXMaterial:
	def __init__(self, shaderNode, apiType):
		self.shaderNode = shaderNode
		self.apiType = apiType

	def writeToFile(self, file):
		"""
		Based off http://www.chadvernon.com/blog/resources/maya-tools/cvxporter/
		"""
		global PBRTXTextureCount

		material = PBRTXMaterialStorage()

		material.color = [self.shaderNode.color().r, self.shaderNode.color().g, self.shaderNode.color().b] 
		material.alpha = 1.0 - self.shaderNode.transparency().r
		material.diffuseCoefficient = self.shaderNode.diffuseCoeff()
		if self.apiType == OM.MFn.kLambert:
			material.specular = [0.0, 0.0, 0.0]
			material.roughness = 1
		else:
			material.specular = [self.shaderNode.specularColor().r, self.shaderNode.specularColor().g, self.shaderNode.specularColor().b] 
			material.reflection = map(lambda x: x * self.shaderNode.reflectivity(), material.specular)
			if self.apiType == OM.MFn.kPhong:
				material.roughness = 1/self.shaderNode.cosPower() # todo: these are aproximitions
			elif self.apiType == OM.MFn.kBlinn:
				material.roughness = self.shaderNode.eccentricity() # todo: these are aproximitions

		material.emissive = [self.shaderNode.incandescence().r, self.shaderNode.incandescence().g, self.shaderNode.incandescence().b]
		
		# Get texture
		plugColor = self.shaderNode.findPlug('color')
		plugsToColor = OM.MPlugArray()
		plugColor.connectedTo(plugsToColor, True, False)
		
		if plugsToColor.length():
			oSource = plugsToColor[0].node()
			if oSource.hasFn(OM.MFn.kFileTexture):
				fnDepNodeFile = OM.MFnDependencyNode(oSource)
				plugTexture = fnDepNodeFile.findPlug('fileTextureName')
				fileTextureName = plugTexture.asString()

				PBRTXTextureCount = PBRTXTextureCount + 1
				material.texture = "Texture%d" % (PBRTXTextureCount)

				cWrite(file, 'Texture  "%s" "color" "imagemap" "string filename" ["%s"]\n' % (material.texture, fileTextureName))

		# Get bump map
		plugBump = self.shaderNode.findPlug('normalCamera')
		plugsToBump = OM.MPlugArray()
		plugBump.connectedTo(plugsToBump, True, False)
		
		if plugsToBump.length():
			oSource = plugsToBump[0].node()

			if oSource.apiType() == OM.MFn.kBump:
				fnDepNodeBump = OM.MFnDependencyNode(oSource)
				plugToValue = fnDepNodeBump.findPlug('bumpValue')
				plugsToValue = OM.MPlugArray()
				plugToValue.connectedTo(plugsToValue, True, False)

				if plugsToValue.length():
					vSource = plugsToValue[0].node()

					if vSource.apiType() == OM.MFn.kFileTexture:
						fnDepNodeFile = OM.MFnDependencyNode(vSource)
						plugTexture = fnDepNodeFile.findPlug('fileTextureName')
						fileTextureName = plugTexture.asString()

						PBRTXTextureCount = PBRTXTextureCount + 1
						material.bumpmap = "Texture%d" % (PBRTXTextureCount)

						cWrite(file, 'Texture  "%s" "float" "imagemap" "string filename" ["%s"]\n' % (material.bumpmap, fileTextureName))
		
		cWrite(file, 'Material "uber" ')
		# Texture turns color black
		if material.texture:
			cWritePartial(file, '"texture Kd" ["%s"] ' % material.texture)
		else:
			cWritePartial(file, '"color Kd" [%+f %+f %+f] ' % (material.color[0], material.color[1], material.color[2]))
		if material.bumpmap:
			cWritePartial(file, '"texture bumpmap" ["%s"] ' % material.bumpmap)
		cWritePartial(file, '"float roughness" [%+f] ' % material.roughness)
		cWritePartial(file, '"color Ks" [%+f %+f %+f] ' % (material.specular[0], material.specular[1], material.specular[2]))
		cWritePartial(file, '"color Kr" [%+f %+f %+f] ' % (material.reflection[0], material.reflection[1], material.reflection[2]))
		cWritePartial(file, '\n')

class PBRTXMatteMaterial:
	def __init__(self, shaderNode):
		self.shaderNode = shaderNode

	def writeToFile(self, file):
		kd = self.shaderNode.findPlug('Kd').asMDataHandle().asFloatVector()
		sigma = self.shaderNode.findPlug('sigma').asFloat()

		cWrite(file, 'Material "matte" ')
		cWritePartial(file, '"color Kd" [%f %f %f] ' % (kd[0], kd[1], kd[2]))
		cWritePartial(file, '"float sigma" [%f]\n' % sigma)

class PBRTXDifferentialMatteMaterial:
	def __init__(self, shaderNode):
		self.shaderNode = shaderNode

	def writeToFile(self, file):
		kd = self.shaderNode.findPlug('Kd').asMDataHandle().asFloatVector()
		sigma = self.shaderNode.findPlug('sigma').asFloat()

		cWrite(file, 'Material "differentialmatte" ')
		cWritePartial(file, '"color Kd" [%f %f %f] ' % (kd[0], kd[1], kd[2]))
		cWritePartial(file, '"float sigma" [%f]\n' % sigma)

class PBRTXGlassMaterial:
	def __init__(self, shaderNode):
		self.shaderNode = shaderNode

	def writeToFile(self, file):
		kr = self.shaderNode.findPlug('Kr').asMDataHandle().asFloatVector()
		kt = self.shaderNode.findPlug('Kt').asMDataHandle().asFloatVector()
		ior = self.shaderNode.findPlug('ior').asFloat()

		cWrite(file, 'Material "glass" ')
		cWritePartial(file, '"color Kr" [%f %f %f] ' % (kr[0], kr[1], kr[2]))
		cWritePartial(file, '"color Kt" [%f %f %f] ' % (kt[0], kt[1], kt[2]))
		cWritePartial(file, '"float index" [%f]\n' % ior)

class PBRTXPlanetMaterial:
	def __init__(self, shaderNode):
		self.shaderNode = shaderNode

	def writeToFile(self, file):
		cWrite(file, 'Material "planet" ')

		global kPlanetPBRTShaderFloatKeys
		for key in kPlanetPBRTShaderFloatKeys:
			value = self.shaderNode.findPlug(key).asFloat()
			cWritePartial(file, '"float %s" [%f] ' % (key, value))
		cWritePartial(file, '\n')

class PBRTXUVMaterial:
	def __init__(self, shaderNode):
		self.shaderNode = shaderNode

	def writeToFile(self, file):
		global PBRTXTextureCount
		PBRTXTextureCount = PBRTXTextureCount + 1
		texturename = 'Texture%d' % PBRTXTextureCount
		cWrite(file, 'Texture "%s" "spectrum" "uv" ' % texturename)

		global kUVPBRTShaderFloatKeys
		for key in kUVPBRTShaderFloatKeys:
			value = self.shaderNode.findPlug(key).asFloat()
			cWritePartial(file, '"float %s" [%f] ' % (key, value))

		cWritePartial(file, '\n')
		cWrite(file, 'Material "matte" "texture Kd" ["%s"]\n' % texturename)

def findShader(obj):
	fnNode = OM.MFnDependencyNode(obj)
	shaderPlug = fnNode.findPlug("surfaceShader")
	
	if not shaderPlug.isNull():
		connectedPlugs = OM.MPlugArray()
		shaderPlug.connectedTo(connectedPlugs, True, False)

		if not connectedPlugs.length() == 1:
			OM.MGlobal.displayWarning("Error getting shader")
		else:
			return connectedPlugs[0].node()

	return None

def materialFactory(shaderNode):
	if shaderNode is None:
		return None
	
	if shaderNode.apiType() == OM.MFn.kPhong:
		return PBRTXMaterial(OM.MFnPhongShader(shaderNode), OM.MFn.kPhong)
	elif shaderNode.apiType() == OM.MFn.kBlinn:
		return PBRTXMaterial(OM.MFnBlinnShader(shaderNode), OM.MFn.kBlinn)
	elif shaderNode.apiType() == OM.MFn.kLambert:
		return PBRTXMaterial(OM.MFnLambertShader(shaderNode), OM.MFn.kLambert)
	elif shaderNode.apiType() == OM.MFn.kSurfaceShader:
		shader = OM.MFnDependencyNode(shaderNode)
		plugColor = shader.findPlug('outColor')
		plugsToColor = OM.MPlugArray()
		plugColor.connectedTo(plugsToColor, True, False)

		if plugsToColor.length():
			oSource = plugsToColor[0].node()
			fnDepNode = OM.MFnDependencyNode(oSource)
			if fnDepNode.name()[0:len(kMattePBRTShader)] == kMattePBRTShader:
				return PBRTXMatteMaterial(fnDepNode)
			elif fnDepNode.name()[0:len(kGlassPBRTShader)] == kGlassPBRTShader:
				return PBRTXGlassMaterial(fnDepNode)
			elif fnDepNode.name()[0:len(kPlanetPBRTShader)] == kPlanetPBRTShader:
				return PBRTXPlanetMaterial(fnDepNode)
			elif fnDepNode.name()[0:len(kUVPBRTShader)] == kUVPBRTShader:
				return PBRTXUVMaterial(fnDepNode)
			elif fnDepNode.name()[0:len(kDifferentialMattePBRTShader)] == kDifferentialMattePBRTShader:
				return PBRTXDifferentialMatteMaterial(fnDepNode)

		return None
	else:
		OM.MGlobal.displayWarning("Unsupported shader type found for %i" % shaderNode.apiType())
		OM.MGlobal.displayWarning("Unsupported shader type found for %s" % shaderNode.name())
	
################################################
# meshes
################################################

class PBRTXPolygonSet:
	def __init__(self, dagPath, component, set):
		self.dagPath = OM.MDagPath(dagPath)
		self.component = OM.MObject(component)
		self.material = materialFactory(set)
		
	def getLocalIndex(self, verticiesFromGetVerticies, verticiesFromGetTriangle):
		"""
		From PBRTMaya
		
		MItMeshPolygon::getTriangle() returns object-relative vertex
		indices; BUT MItMeshPolygon::normalIndex() and ::getNormal() need
		face-relative vertex indices! This converts vertex indices from
		object-relative to face-relative.
		param  getVertices: Array of object-relative vertex indices for
							entire face.
		param  getTriangle: Array of object-relative vertex indices for
							local triangle in face.
		return Array of face-relative indicies for the specified vertices.
				Number of elements in returned array == number in getTriangle
				(should be 3).
		note	If getTriangle array does not include a corresponding vertex
				in getVertices array then a value of (-1) will be inserted
				in that position within the returned array.
		"""
		localIndex = OM.MIntArray()

		for gt in xrange(verticiesFromGetTriangle.length()):
			for gv in xrange(verticiesFromGetVerticies.length()):
				if verticiesFromGetTriangle[gt] == verticiesFromGetVerticies[gv]:
					localIndex.append(gv)
					break

			# if nothing was added, add default "no match"
			if localIndex.length() == gt:
				localIndex.append(-1)

		return localIndex
		
	def writeToFile(self, file):
		points = []
		normals = []
		uvs = []
		
		mesh = OM.MFnMesh(self.dagPath)
		itMeshPolygon = OM.MItMeshPolygon(self.dagPath, self.component)
		
		# cache the points
		meshPoints = OM.MPointArray()
		mesh.getPoints(meshPoints, OM.MSpace.kWorld)

		# cache normals for each vertex
		meshNormals = OM.MFloatVectorArray()
		# Normals are per-vertex per-face..
		# use MItMeshPolygon::normalIndex() for index
		mesh.getNormals(meshNormals, OM.MSpace.kWorld)

		# Get UVSets for this mesh
		uvSets = []
		mesh.getUVSetNames(uvSets)
		uvSet = uvSets[0]

		cWrite(file, 'AttributeBegin\n')
		cIndent()
		
		if not self.material is None:
			self.material.writeToFile(file)
			cWrite(file, '\n')
		
		cWrite(file, 'Shape "trianglemesh"\n')

		count = 0

		# get the indices for the triangles in the mesh
		while not itMeshPolygon.isDone():
			if itMeshPolygon.hasValidTriangulation():
				polygonVerticieIndexes = OM.MIntArray()
				# this gives us the indexes into meshpoints of the vertices in this polygon (object reletive)
				itMeshPolygon.getVertices(polygonVerticieIndexes)

				triangleVerticies = OM.MPointArray()
				triangleVerticieIndexes = OM.MIntArray()
				# this will put the vertices of this polygon into triangleVerticies (which we dont care about) and
				# the indexes into meshpoints of the vertices in this **triangulated** polygon (object reletive)
				itMeshPolygon.getTriangles(triangleVerticies, triangleVerticieIndexes, OM.MSpace.kWorld)

				localUs = OM.MFloatArray()
				localVs = OM.MFloatArray()
				itMeshPolygon.getUVs(localUs, localVs, uvSet)

				# figure out face local triangulated verticie indexes i.e. convert object reletive indexes from
				# triangleVerticieIndexes to face reletive indexes
				localTriangulatedVerticieIndexes = self.getLocalIndex(polygonVerticieIndexes, triangleVerticieIndexes)

				for i in xrange(triangleVerticieIndexes.length()):
					count = count+1

					meshVerticieIndex = triangleVerticieIndexes[i]
					localVerticieIndex = localTriangulatedVerticieIndexes[i]

					verticie = meshPoints[meshVerticieIndex]
					points.append(verticie.x)
					points.append(verticie.y)
					points.append(verticie.z)

					normalIndex = itMeshPolygon.normalIndex(localVerticieIndex)
					normal = meshNormals[normalIndex]
					normals.append(normal.x)
					normals.append(normal.y)
					normals.append(normal.z)

					uvs.append(localUs[localVerticieIndex])
					uvs.append(localVs[localVerticieIndex]) # todo: dont know why, but this works
			else:
				OM.MGlobal.displayError("MItMeshPolygon::hasValidTriangulation")
			
			itMeshPolygon.next()
		
		cWrite(file, '\t"point P" [ ')
		for p in points:
			cWritePartial(file, '%+f ' % (p))
		cWritePartial(file, ']\n')
		
		cWrite(file, '\t"normal N" [ ')
		for n in normals:
			cWritePartial(file, '%+f ' % (n))	
		cWritePartial(file, ']\n')
		
		cWrite(file, '\t"float uv" [ ')
		for uv in uvs:
			cWritePartial(file, '%+f ' % (uv))		
		cWritePartial(file, ']\n')
		
		cWrite(file, '\t"integer indices" [ ')
		for i in xrange(count):
			cWritePartial(file, '%d ' % (i))
		cWritePartial(file, ']\n')
		
		cUnindent()
		cWrite(file, 'AttributeEnd\n')

class PBRTXMesh:
	def __init__(self, dagPath):
		self.dagPath = OM.MDagPath(dagPath)
		self.polySets = []
		self.getMeshData()
				
	def getMeshData(self):
		self.polySets = []
		dagPath = OM.MDagPath(self.dagPath)
		
		fPolygonSets = OM.MObjectArray()
		fPolygonComponents = OM.MObjectArray()
		
		fMesh = OM.MFnMesh(dagPath)
		
		# Have to make the path include the shape below it so that
		# we can determine if the underlying shape node is instanced.
		# By default, dag paths only include transform nodes.
		dagPath.extendToShape()

		# If the shape is instanced then we need to determine which instance this path refers to.
		instanceNum = 0
		if dagPath.isInstanced():
			instanceNum = dagPath.instanceNumber()

		# Get the connected sets and members - these will be used to determine texturing of different faces
		try:
			fMesh.getConnectedSetsAndMembers(instanceNum, fPolygonSets, fPolygonComponents, True)
		except:
			OM.MGlobal.displayError("MFnMesh::getConnectedSetsAndMembers")
			return
		
		setCount = fPolygonSets.length()
		if setCount > 1:
			setCount = setCount - 1
		
		for i in xrange(0, setCount):
			self.polySets.append(PBRTXPolygonSet(dagPath, fPolygonComponents[i], findShader(fPolygonSets[i])))
	
	def writeToFile(self, file):
		for poly in self.polySets:
			poly.writeToFile(file)

################################################
# NURBS
################################################

class PBRTXNURBS:
	def __init__(self, dagPath):
		self.dagPath = OM.MDagPath(dagPath)
		self.nurb = OM.MFnNurbsSurface(self.dagPath)
				
	def writeToFile(self, file):
		cWrite(file, 'AttributeBegin\n')
		cIndent()

		# TODO: export material

		cWrite(file, 'Shape "nurbs"\n')

		numCVUs = self.nurb.numCVsInU()
		numCVVs = self.nurb.numCVsInV()
		degreeU = self.nurb.degreeU()
		degreeV = self.nurb.degreeV()

		cWrite(file, '\t"integer nu" [%i]\n' % numCVUs)
		cWrite(file, '\t"integer nv" [%i]\n' % numCVVs)
		cWrite(file, '\t"integer uorder" [%i]\n' % (degreeU + 1))
		cWrite(file, '\t"integer vorder" [%i]\n' % (degreeV + 1))

		cWrite(file, '\t"float uknots" [ ')
		uknots = OM.MDoubleArray()
		self.nurb.getKnotsInU(uknots)
		cWritePartial(file, '%+f ' % (uknots[0] - (uknots[1] - uknots[0])))
		for i in xrange(0, uknots.length()):
			cWritePartial(file, '%+f ' % uknots[i])
		cWritePartial(file, '%+f ' % (uknots[uknots.length()-1] + (uknots[uknots.length()-1] - uknots[uknots.length()-2])))
		cWritePartial(file, ']\n')

		cWrite(file, '\t"float vknots" [ ')
		vknots = OM.MDoubleArray()
		self.nurb.getKnotsInV(vknots)
		cWritePartial(file, '%+f ' % (vknots[0] - (vknots[1] - vknots[0])))
		for i in xrange(0, vknots.length()):
			cWritePartial(file, '%+f ' % vknots[i])
		cWritePartial(file, '%+f ' % (vknots[vknots.length()-1] + (vknots[vknots.length()-1] - vknots[vknots.length()-2])))
		cWritePartial(file, ']\n')

		# can't use getKnotDomain as it's broken
		# this means that we don't support partial surfaces
		startU = uknots[degreeU-1]
		endU = uknots[uknots.length()-degreeU]
		startV = vknots[degreeV-1]
		endV = vknots[vknots.length()-degreeV]

		cWrite(file, '\t"float u0" [%+f]\n' % startU)
		cWrite(file, '\t"float u1" [%+f]\n' % endU)
		cWrite(file, '\t"float v0" [%+f]\n' % startV)
		cWrite(file, '\t"float v1" [%+f]\n' % endV)

		cWrite(file, '\t"point P" [ ')
		cvs = OM.MPointArray()
		self.nurb.getCVs(cvs, OM.MSpace.kWorld)
		nl = False
		for u in xrange(numCVUs):
			if nl:
				cWrite(file, "\t			")
			nl = True
			gap = ""
			for v in xrange(numCVVs):
				i = numCVVs * u + v
				cWritePartial(file, gap + "%+f %+f %+f" % (cvs[i][0], cvs[i][1], cvs[i][2]))
				gap = "	   "
			if u != numCVUs-1:
				cWritePartial(file, "\n")
		cWritePartial(file, ' ]\n')

		cUnindent()
		cWrite(file, 'AttributeEnd\n')

################################################
# Fluids
################################################

class PBRTXFluid:
	def __init__(self, dagPath):
		self.dagPath = OM.MDagPath(dagPath)
		self.fluid = OMFx.MFnFluid(self.dagPath)
				
	def writeToFile(self, file):
		cWrite(file, 'AttributeBegin\n')
		cIndent()

		writeTransformationMatrix(file, self.dagPath)
		
		cWrite(file, 'Volume "volumegrid" ')
		
		nxPtr = uintPtr()
		nyPtr = uintPtr()
		nzPtr = uintPtr()
		self.fluid.getResolution(nxPtr[0], nyPtr[0], nzPtr[0])
		nx = uintFromPtr(nxPtr)
		ny = uintFromPtr(nyPtr)
		nz = uintFromPtr(nzPtr)

		cWritePartial(file, '"integer nx" [%i] ' % nx)
		cWritePartial(file, '"integer ny" [%i] ' % ny)
		cWritePartial(file, '"integer nz" [%i] ' % nz)

		sxPtr = doublePtr()
		syPtr = doublePtr()
		szPtr = doublePtr()
		self.fluid.getDimensions(sxPtr[0], syPtr[0], szPtr[0])
		sx = doubleFromPtr(sxPtr)
		sy = doubleFromPtr(syPtr)
		sz = doubleFromPtr(szPtr)

		cWritePartial(file, '"point p0" [%f %f %f] ' % (-sx/2.0, -sy/2.0, -sz/2.0))
		cWritePartial(file, '"point p1" [%f %f %f] ' % (sx/2.0, sy/2.0, sz/2.0))
		
		# get density data
		densities = self.fluid.density()
		util = OM.MScriptUtil(densities)

		cWritePartial(file, '"float density" [')
		sep = ""
		for x in xrange(0, nx):
			for y in xrange(0, ny):
				for z in xrange(0, nz):
					index = self.fluid.index(x, y, z, nx, ny, nz)
					density = util.getFloatArrayItem(densities, index)
					
					cWritePartial(file, '%s%f' % (sep, density))
					sep = " "
		
		cWritePartial(file, ']\n')

		cUnindent()
		cWrite(file, 'AttributeEnd\n')

################################################
# Exporter
################################################

def exportToPath(spec):
	start = time()
	print "Starting export to: %s" % spec.path

	# open export file
	out = open(spec.path, "w")

	# temporary variables
	tempDagPath = OM.MDagPath()

	# write the camera info
	cam = PBRTXCamera(spec)
	cam.writeToFile(out)

	# write the film info
	cWrite(out, 'Film "image" "integer xresolution" [%d] "integer yresolution" [%d] "string filename" ["%s"]\n' % (spec.width, spec.height, spec.imageName))

	# begin the world
	cWrite(out, '\n')
	cWrite(out, 'WorldBegin\n')
	cIndent()

	# loop through all the lights
	lightItDag = OM.MItDag(OM.MItDag.kDepthFirst, OM.MFn.kLight)
	while not lightItDag.isDone():
		lightItDag.getPath(tempDagPath)
		
		# visibility test
		visTest = OM.MFnDagNode(tempDagPath)
		if isVisible(visTest):
			light = lightFactory(tempDagPath)
			if not light is None:
				light.writeToFile(out)
				
		lightItDag.next()
		
	# loop through all the meshes
	meshItDag = OM.MItDag(OM.MItDag.kDepthFirst, OM.MFn.kMesh)
	while not meshItDag.isDone():
		meshItDag.getPath(tempDagPath)
		
		# visiblity test
		visTest = OM.MFnDagNode(tempDagPath)
		if isVisible(visTest):
			mesh = PBRTXMesh(tempDagPath)
			mesh.writeToFile(out)
		
		meshItDag.next()

	# loop through all the surfaces
	nurbsItDag = OM.MItDag(OM.MItDag.kDepthFirst, OM.MFn.kNurbsSurface)
	while not nurbsItDag.isDone():
		nurbsItDag.getPath(tempDagPath)
		
		# visiblity test
		visTest = OM.MFnDagNode(tempDagPath)
		if isVisible(visTest):
			nurbs = PBRTXNURBS(tempDagPath)
			nurbs.writeToFile(out)
		
		nurbsItDag.next()

	# loop through all the fluids
	fluidItDag = OM.MItDag(OM.MItDag.kDepthFirst, OM.MFn.kFluid)
	while not fluidItDag.isDone():
		fluidItDag.getPath(tempDagPath)
		
		# visiblity test
		visTest = OM.MFnDagNode(tempDagPath)
		if isVisible(visTest):
			fluid = PBRTXFluid(tempDagPath)
			fluid.writeToFile(out)
		
		fluidItDag.next()

	# end the world
	cUnindent()
	cWrite(out, 'WorldEnd')

	# close export file
	out.close()

	print "Export took: %0.2fs" % (time()-start)

################################################
# PBRT executer
################################################		

def displayRenderResults(spec):
	"""
	Based off based off http://www.mail-archive.com/python_inside_maya@googlegroups.com/msg04775.html
	"""
	image = OM.MImage()
	isHdr = 'exr' in spec.imageName

	try:
		image.readFromFile(spec.imageName)

		scriptUtil = OM.MScriptUtil()		
		size = spec.width * spec.height
		rv_pixels = OMR.RV_PIXEL()
		charPixelPtr = image.pixels()

		for i in xrange(size):			
			redIndex = (i*4) + 0
			greenIndex = (i*4) + 1
			blueIndex = (i*4) + 2
			alphaIndex = (i*4) + 3

			red = scriptUtil.getUcharArrayItem(charPixelPtr, redIndex)
			green = scriptUtil.getUcharArrayItem(charPixelPtr, greenIndex)
			blue = scriptUtil.getUcharArrayItem(charPixelPtr, blueIndex)
			alpha = scriptUtil.getUcharArrayItem(charPixelPtr, alphaIndex)

			y = int(i/spec.width)
			x = int(spec.width-(i-y*spec.width))

			rv_pixels.a = alpha
			rv_pixels.r = red
			rv_pixels.g = green
			rv_pixels.b = blue

			OMR.MRenderView.updatePixels(x, x, y, y, rv_pixels, isHdr)

		OMR.MRenderView.refresh(0, spec.width-1, 0, spec.height-1)
	except:
		raise
		OM.MGlobal.displayError("Error displaying render result")

def renderFile(spec):
	start = time()
	print "Starting render of: %s" % spec.path
	OMR.MRenderView.startRender(spec.width, spec.height)

	process = Popen(['bash -c "source '+os.environ['HOME']+'/.bash_profile; pbrt --quiet '+spec.path+'"'], shell=True)
	
	exit_code = None
	while exit_code == None:
		try:
			exit_code = process.wait()
		except OSError as ose:
			if ose.errno != errno.EINTR:
			  raise ose

	if exit_code == 0:
		displayRenderResults(spec)
	else:
		OM.MGlobal.displayError("Render failed")

	OMR.MRenderView.endRender()
	Popen(['rm',spec.imageName])
	print "Render took: %0.2fs" % (time()-start)

################################################
# Command setup
################################################

class PBRTExportCommand(OMPx.MPxCommand):
	def doIt(self, thing):
		self.redoIt()

	def redoIt(self):
		global kDefaultExportSpec
		exportToPath(kDefaultExportSpec)

class PBRTExportAnimationCommand(OMPx.MPxCommand):
	def doIt(self, thing):
		self.redoIt()

	def redoIt(self):
		totalFrames = int(OMA.MAnimControl.maxTime().asUnits(OM.MTime.kFilm))
		
		for currentFrame in range(totalFrames):
			frame = OM.MTime(currentFrame + 1) 
			OMA.MAnimControl.setCurrentTime(frame)
			exportToPath(kAnimatedExportSpec(currentFrame))	
		
class PBRTExportRenderCommand(OMPx.MPxCommand):
	def doIt(self, thing):
		self.redoIt()

	def redoIt(self):
		global kDefaultExportSpec
		exportToPath(kDefaultExportSpec)
		renderFile(kDefaultExportSpec)

class PBRTApplyShaderCommand(OMPx.MPxCommand):
	def __init__(self, shaderName):
		self.shaderName = shaderName
		OMPx.MPxCommand.__init__(self)

	def doIt(self, thing):
		selected = cmds.ls(sl=True)

		self.shader = cmds.shadingNode(self.shaderName, asUtility=True)

		# Create a surface shader to which we will attach our depth shader.
		self.surfaceShaderName = cmds.shadingNode('surfaceShader', asShader=True)
		cmds.connectAttr(self.shader + '.outColor', self.surfaceShaderName + '.outColor', force=True)

		# Create a shading group to tie everything together.
		self.shadingGroupName = cmds.sets(name='surfaceShader1SG', empty=True, noSurfaceShader=True, renderable=True)
		cmds.connectAttr(self.surfaceShaderName + '.outColor', self.shadingGroupName + '.surfaceShader', force=True)

		cmds.select(selected)

		self.redoIt()

	def redoIt(self):
		cmds.sets(cmds.ls(sl=True), e=True, forceElement=self.shadingGroupName)
	
def createCommandExporter():
	return OMPx.asMPxPtr(PBRTExportCommand())
	
def createCommandAnimationExporter():
	return OMPx.asMPxPtr(PBRTExportAnimationCommand())

def createCommandExporterRenderer():
	return OMPx.asMPxPtr(PBRTExportRenderCommand())

def createCommandMatte():
	return OMPx.asMPxPtr(PBRTApplyShaderCommand(kMattePBRTShader))

def createCommandGlass():
	return OMPx.asMPxPtr(PBRTApplyShaderCommand(kGlassPBRTShader))

def createCommandPlanet():
	return OMPx.asMPxPtr(PBRTApplyShaderCommand(kPlanetPBRTShader))

def createCommandUV():
	return OMPx.asMPxPtr(PBRTApplyShaderCommand(kUVPBRTShader))

def createCommandDifferentialMatte():
	return OMPx.asMPxPtr(PBRTApplyShaderCommand(kDifferentialMattePBRTShader))

################################################
# Material plug-ins
################################################

class PBRTMatteMaterialShaderNode(OMPx.MPxNode):
	def compute(self, pPlug, pDataBlock):
		None

class PBRTGlassMaterialShaderNode(OMPx.MPxNode):
	def compute(self, pPlug, pDataBlock):
		None

class PBRTPlanetMaterialShaderNode(OMPx.MPxNode):
	def compute(self, pPlug, pDataBlock):
		None

class PBRTUVMaterialShaderNode(OMPx.MPxNode):
	def compute(self, pPlug, pDataBlock):
		None

class PBRTDifferentialMatteMaterialShaderNode(OMPx.MPxNode):
	def compute(self, pPlug, pDataBlock):
		None

################################################
# Material plug-in setup
################################################

def matteNodeCreator():
	return OMPx.asMPxPtr(PBRTMatteMaterialShaderNode())

def glassNodeCreator():
	return OMPx.asMPxPtr(PBRTGlassMaterialShaderNode())

def planetNodeCreator():
	return OMPx.asMPxPtr(PBRTPlanetMaterialShaderNode())

def uvNodeCreator():
	return OMPx.asMPxPtr(PBRTUVMaterialShaderNode())

def differentialMatteNodeCreator():
	return OMPx.asMPxPtr(PBRTDifferentialMatteMaterialShaderNode())

def matteNodeInitializer():
	# Create a numeric attribute function set, since our attributes will all be defined by numeric types.
	numericAttributeFn = OM.MFnNumericAttribute()

	global kDefaultKd
	diffuseColorAttribute = numericAttributeFn.createColor('diffuseColor', 'Kd')
	numericAttributeFn.setStorable(True)
	numericAttributeFn.setDefault(kDefaultKd[0], kDefaultKd[1], kDefaultKd[2])
	PBRTMatteMaterialShaderNode.addAttribute(diffuseColorAttribute)

	global kDefaultSigma, kMinSigma, kMaxSigma
	sigmaAttribute = numericAttributeFn.create('sigma', 'sigma', OM.MFnNumericData.kFloat, kDefaultSigma)
	numericAttributeFn.setStorable(True)
	numericAttributeFn.setMin(kMinSigma)
	numericAttributeFn.setMax(kMaxSigma)
	PBRTMatteMaterialShaderNode.addAttribute(sigmaAttribute)

	outColorAttribute = numericAttributeFn.createColor('outColor', 'oc')
	numericAttributeFn.setStorable(False)
	numericAttributeFn.setWritable(False)
	numericAttributeFn.setReadable(True)
	numericAttributeFn.setHidden(False)
	PBRTMatteMaterialShaderNode.addAttribute(outColorAttribute)

def glassNodeInitializer():
	# Create a numeric attribute function set, since our attributes will all be defined by numeric types.
	numericAttributeFn = OM.MFnNumericAttribute()

	global kDefaultKr
	krAttribute = numericAttributeFn.createColor('reflectedColor', 'Kr')
	numericAttributeFn.setStorable(True)
	numericAttributeFn.setDefault(kDefaultKr[0], kDefaultKr[1], kDefaultKr[2])
	PBRTGlassMaterialShaderNode.addAttribute(krAttribute)

	global kDefaultKt
	ktAttribute = numericAttributeFn.createColor('transmittedColor', 'Kt')
	numericAttributeFn.setStorable(True)
	numericAttributeFn.setDefault(kDefaultKt[0], kDefaultKt[1], kDefaultKt[2])
	PBRTGlassMaterialShaderNode.addAttribute(ktAttribute)

	global kDefaultIOR, kMinIOR, kMaxIOR
	iorAttribute = numericAttributeFn.create('indexOfRefraction', 'ior', OM.MFnNumericData.kFloat, kDefaultIOR)
	numericAttributeFn.setStorable(True)
	numericAttributeFn.setMin(kMinIOR)
	numericAttributeFn.setMax(kMaxIOR)
	PBRTGlassMaterialShaderNode.addAttribute(iorAttribute)

	outColorAttribute = numericAttributeFn.createColor('outColor', 'oc')
	numericAttributeFn.setStorable(False)
	numericAttributeFn.setWritable(False)
	numericAttributeFn.setReadable(True)
	numericAttributeFn.setHidden(False)
	PBRTGlassMaterialShaderNode.addAttribute(outColorAttribute)

def floatOnlyNodeInitializer(classPtr, keys, defaults, mins, maxes):
	# Create a numeric attribute function set, since our attributes will all be defined by numeric types.
	numericAttributeFn = OM.MFnNumericAttribute()

	for i in xrange(len(keys)):
		key = keys[i]
		default = defaults[i]
		minVal = mins[i]
		maxVal = maxes[i]

		attribute = numericAttributeFn.create(key, key, OM.MFnNumericData.kFloat, default)
		numericAttributeFn.setStorable(True)
		numericAttributeFn.setMin(minVal)
		numericAttributeFn.setMax(maxVal)
		classPtr.addAttribute(attribute)

	outColorAttribute = numericAttributeFn.createColor('outColor', 'oc')
	numericAttributeFn.setStorable(False)
	numericAttributeFn.setWritable(False)
	numericAttributeFn.setReadable(True)
	numericAttributeFn.setHidden(False)
	classPtr.addAttribute(outColorAttribute)

def planetNodeInitializer():
	global kPlanetPBRTShaderFloatKeys, kPlanetPBRTShaderFloatDefaults, kPlanetPBRTShaderFloatMaxes, kPlanetPBRTShaderFloatMins
	floatOnlyNodeInitializer(PBRTPlanetMaterialShaderNode, kPlanetPBRTShaderFloatKeys, kPlanetPBRTShaderFloatDefaults, kPlanetPBRTShaderFloatMins, kPlanetPBRTShaderFloatMaxes)

def uvNodeInitializer():
	global kUVPBRTShaderFloatKeys, kUVPBRTShaderFloatDefaults, kUVPBRTShaderFloatMaxes, kUVPBRTShaderFloatMins
	floatOnlyNodeInitializer(PBRTUVMaterialShaderNode, kUVPBRTShaderFloatKeys, kUVPBRTShaderFloatDefaults, kUVPBRTShaderFloatMins, kUVPBRTShaderFloatMaxes)

################################################
# Plugin setup
################################################

def initializePlugin(mobject):
	''' Initializes the plug-in. '''
	mplugin = OMPx.MFnPlugin(mobject)
	try:
		mplugin.registerCommand("pbrtexport", createCommandExporter)
		mplugin.registerCommand("pbrtexportanimation", createCommandAnimationExporter)
		mplugin.registerCommand("pbrtexportandrender", createCommandExporterRenderer)
		mplugin.registerCommand("pbrtapplymatte", createCommandMatte)
		mplugin.registerCommand("pbrtapplyglass", createCommandGlass)
		mplugin.registerCommand("pbrtapplyplanet", createCommandPlanet)
		mplugin.registerCommand("pbrtapplyuv", createCommandUV)
		mplugin.registerCommand("pbrtapplydifferentialmatte", createCommandDifferentialMatte)

		mplugin.registerNode(kMattePBRTShader, kMattePBRTShaderNodeId, matteNodeCreator, 
				   			 matteNodeInitializer, OMPx.MPxNode.kDependNode, kPluginNodeClassify)
		mplugin.registerNode(kGlassPBRTShader, kGlassPBRTShaderNodeId, glassNodeCreator, 
				   			 glassNodeInitializer, OMPx.MPxNode.kDependNode, kPluginNodeClassify)
		mplugin.registerNode(kPlanetPBRTShader, kPlanetPBRTShaderNodeId, planetNodeCreator, 
				   			 planetNodeInitializer, OMPx.MPxNode.kDependNode, kPluginNodeClassify)
		mplugin.registerNode(kUVPBRTShader, kUVPBRTShaderNodeId, uvNodeCreator, 
				   			 uvNodeInitializer, OMPx.MPxNode.kDependNode, kPluginNodeClassify)
		mplugin.registerNode(kDifferentialMattePBRTShader, kDifferentialMattePBRTShaderNodeId, differentialMatteNodeCreator, 
				   			 matteNodeInitializer, OMPx.MPxNode.kDependNode, kPluginNodeClassify) # use matteNodeInitializer as differential matte is the same as matte
	except:
		OM.MGlobal.displayError("Failed to register plugins")
		raise

def uninitializePlugin(mobject):
	''' Unitializes the plug-in. '''
	mplugin = OMPx.MFnPlugin(mobject)
	try:
		mplugin.deregisterCommand("pbrtexport")
		mplugin.deregisterCommand("pbrtexportanimation")
		mplugin.deregisterCommand("pbrtexportandrender")
		mplugin.deregisterCommand("pbrtapplymatte")
		mplugin.deregisterCommand("pbrtapplyglass")
		mplugin.deregisterCommand("pbrtapplyplanet")
		mplugin.deregisterCommand("pbrtapplyuv")
		mplugin.deregisterCommand("pbrtapplydifferentialmatte")

		mplugin.deregisterNode(kMattePBRTShaderNodeId)
		mplugin.deregisterNode(kGlassPBRTShaderNodeId)
		mplugin.deregisterNode(kPlanetPBRTShaderNodeId)
		mplugin.deregisterNode(kUVPBRTShaderNodeId)
		mplugin.deregisterNode(kDifferentialMattePBRTShaderNodeId)
	except:
		OM.MGlobal.displayError("Failed to deregister plugins")
		raise
